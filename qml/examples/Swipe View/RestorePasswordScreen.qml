import QtQuick 2.10
import QtQuick.Controls 2.10
import QtQuick.Layouts 1.2

SwipeScreen {
  onSelected: emailTextField.forceActiveFocus()

  ColumnLayout {
    anchors.centerIn: parent

    RowLayout {
      Label { text: "Email" }
      TextField {
        id: emailTextField
        Layout.fillWidth: true
      }
    }

    Button {
      Layout.fillWidth: true
      text: "Send instructions"
    }
  }
}
