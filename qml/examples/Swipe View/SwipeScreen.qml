import QtQuick 2.10

Item {
  property bool isFirstScreen: false
  property bool isLastScreen: false
  signal selected()
}
